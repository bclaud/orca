{ pkgs }:

with pkgs;

let
  pname = "orca";
  version = "0.0.1";

  src = ./.;

  mixFodDeps = beamPackages.fetchMixDeps {

    pname = "mix-deps-${pname}";
    inherit src version elixir;
    sha256 = "sha256-t7lnT8Prwxnh8RV+TQi0vDb9eQlRxmfbnnx2/pkfEIY=";

    # set mixEnv to empty make it download deps from all envs
    mixEnv = "";
  };
in

beamPackages.mixRelease {
  inherit mixFodDeps pname version src elixir;

  nativeBuildInputs = [ glibcLocalesUtf8 ];
  
  doCheck = true;

  LC_ALL = "en_US.UTF-8";
  LANG = "en_US.UTF-8";

  # Should be nativeCheckInputs but nothing working atm
  # nativeCheckInputs = [postgresqlTestHook];
  checkInputs = [ postgresql postgresqlTestHook ];

  checkPhase = '' 
  runHook preCheck
  export postgresqlTestSetupCommands=""
  export PGUSER=$(whoami)

  MIX_ENV=test mix test --no-deps-check

  runHook postCheck
  '';
}

